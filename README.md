# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The purpose of this project is to update my Stock Market project from CSE 305.
* The main things I want to update are:
* 1) When setting the price of stocks, adding an option to set the price to the actual cost in the real-life market.
* 1a) When this option is selected, the price of the stock also changes in real time, again based on the actual price
* 2) Using Spring MVC to replace many of the servlet classes.
* 3) Using Dojo Toolkit to update the UI. Or maybe React? Something better than raw JavaScript at the least.
* 4) Updating the color palette and the style sheets. I originally just threw colors together. Once again, IBM's color library to the rescue!
* 5) Maybe getting a server to run this? Or would running it on localhost suffice right now?
* 6) Test Cases!
* Version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact